import React from 'react';
import { AppBar } from 'react-admin';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import { ReactComponent as LogoSVG } from '../platform/assets/images/logo.svg';



const useStyles = makeStyles(theme => ({
    appBar: props => ({
        backgroundColor: theme.palette.grey[100],
        color: theme.palette.primary.main,
    }),
    title: {
        flex: 1,
        textOverflow: 'ellipsis',
        whiteSpace: 'nowrap',
        overflow: 'hidden',
    },
    spacer: {
        flex: 1,
    },
    logo: {
        width: '140px',
    },
}));

const MyAppBar = props => {
    const classes = useStyles(props);
    return (
        <AppBar {...props} className={classes.appBar}>
            <Typography
                variant="h6"
                color="inherit"
                className={classes.title}
                id="react-admin-title"
            />
            <LogoSVG className={classes.logo} />
            <span className={classes.spacer} />
        </AppBar>
    );
};

export default MyAppBar;