/* eslint react/jsx-key: off */
import React from 'react';
import { Admin, Resource } from 'react-admin'; // eslint-disable-line import/no-unresolved
import { Route } from 'react-router-dom';

import authProvider from './authProvider';
import comments from './comments';
import CustomRouteLayout from './customRouteLayout';
import CustomRouteNoLayout from './customRouteNoLayout';
import dataProvider from './dataProvider';
import i18nProvider from './i18nProvider';
import Layout from './layout/Layout';
import posts from './posts';
import users from './users';
import tags from './tags';
import Login from './layout/Login'

import themeMaterial from './themeMaterial';



const App = () => {
    return(
        <Admin
            authProvider={authProvider}
            dataProvider={dataProvider}
            i18nProvider={i18nProvider}
            title="Example Admin"
            layout={Layout}
            loginPage={Login}
            theme={themeMaterial}
            customRoutes={[
                <Route
                    exact
                    path="/custom"
                    component={props => <CustomRouteNoLayout {...props} />}
                    noLayout
                />,
                <Route
                    exact
                    path="/custom2"
                    component={props => <CustomRouteLayout {...props} />}
                />,
            ]}
        >
            {permissions => [
                <Resource name="posts" {...posts} />,
                <Resource name="comments" {...comments} />,
                permissions ? <Resource name="users" {...users} /> : null,
                <Resource name="tags" {...tags} />,
            ]}
        </Admin>
    )
}

export default App;
