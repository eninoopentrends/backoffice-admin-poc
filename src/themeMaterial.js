import theme from './platform/theme';
import { createMuiTheme } from '@material-ui/core/styles';


const themeMaterial = theme => createMuiTheme({
  palette: {
    primary: {
      light: theme.color.primary.light,
      main: theme.color.primary.main,
      dark: theme.color.primary.dark,
      contrastText: theme.color.primary.contrastText,
    },
    secondary: {
      light: theme.color.secondary.light,
      main: theme.color.secondary.main,
      dark: theme.color.secondary.dark,
      contrastText: theme.color.secondary.contrastText,
    },
    error: {
      light: theme.color.error.light,
      main: theme.color.error.main,
      dark: theme.color.error.dark,
      contrastText: theme.color.error.contrastText,
    },
    action: {
      disabled: theme.color.disabled.color,
      disabledBackground: theme.color.disabled.backgroundColor,
    },
  },
  shape: {
    borderRadius: theme.shape.borderRadius,
  },
  typography: {
    htmlFontSize: 16,
    fontFamily: theme.typography.fontFamily,
    fontSize: theme.typography.fontSize,
    h1: {
      fontFamily: theme.typography.fontFamily,
      fontSize: theme.typography.h1.fontSize,
      fontWeight: theme.typography.h1.fontWeight,
    },
    h2: {
      fontFamily: theme.typography.fontFamily,
      fontSize: theme.typography.h2.fontSize,
      fontWeight: theme.typography.h2.fontWeight,
    },
    h3: {
      fontFamily: theme.typography.fontFamily,
      fontSize: theme.typography.h3.fontSize,
      fontWeight: theme.typography.h3.fontWeight,
    },
    h4: {
      fontFamily: theme.typography.fontFamily,
      fontSize: theme.typography.h4.fontSize,
      fontWeight: theme.typography.h4.fontWeight,
    },
    h5: {
      fontFamily: theme.typography.fontFamily,
      fontSize: theme.typography.h5.fontSize,
      fontWeight: theme.typography.h5.fontWeight,
    },
    h6: {
      fontFamily: theme.typography.fontFamily,
      fontSize: theme.typography.h5.fontSize,
      fontWeight: theme.typography.h6.fontWeight,
    },
    button: {
      fontFamily: theme.button.fontFamily,
      fontWeight: theme.button.fontWeight,
      fontSize: theme.button.fontSize,
      lineHeight: theme.button.lineHeight,
      letterSpacing: theme.button.letterSpacing,
      textTransform: theme.button.textTransform,
    },
  },
  overrides: {
    MuiDrawer: {
      paper: {
        minWidth: 256,
      },
      paperAnchorDockedLeft: {
        small: 10,
        medium: 20,
        large: 30,
        borderRight: 'none',
      },
    },
    MuiButton: {
      root: {
        minWidth: theme.button.minWidth,
        color: theme.color.grey.darker,
        '&:hover': {
          color: '#fff',
          backgroundColor: theme.color.grey.darker,
        },
        borderRadius: `${theme.button.borderRadius}px`,
        fontSize: theme.button.fontSize,
        lineHeight: theme.button.lineHeight,
      },
      contained: {
        color: '#fff',
        backgroundColor: theme.color.grey.darker,
        boxShadow: 'none',
        '&:hover': {
          boxShadow: 'none',
          backgroundColor: theme.color.primary.main,
        },
      },
      containedPrimary: {
        '&:hover': {
          backgroundColor: theme.color.primary.light,
        },
      },
      textPrimary: {
        '&:hover': {
          color: '#ffff',
          backgroundColor: theme.color.primary.main,
        },
      },
      textSecondary: {
        '&:hover': {
          color: '#ffff',
          backgroundColor: theme.color.secondary.main,
        },
      },
      outlined: {
        borderColor: theme.color.grey.darker,
      },
      outlinedPrimary: {
        '&:hover': {
          color: '#ffff',
          backgroundColor: theme.color.primary.main,
        },
      },
      outlinedSecondary: {
        '&:hover': {
          color: '#ffff',
          backgroundColor: theme.color.secondary.main,
        },
      },
      sizeLarge: {
        fontSize: theme.button.fontSize,
      },
      sizeSmall: {
        minWidth: '0',
      },
    },
    MuiFormLabel: {
      root: {
        color: theme.color.primary.light,
      },
    },
    MuiFormControl: {
      root: {
        marginTop: '10px',
      },
    },
    MuiInput: {
      root: {
        '&.Mui-error': {
          color: theme.color.error.main,
        },
        '&.Mui-error .MuiIconButton-root': {
          color: theme.color.error.main,
        },
      },
      underline: {
        '&:hover:not($disabled):after': {
          backgroundColor: theme.color.primary.light,
        },
        '&:after': {
          borderBottom: `2px solid ${theme.color.primary.light}`,
        },
        '&:hover:not($disabled):before': {
          borderBottom: `2px solid ${theme.color.primary.dark}`,
        },
      },
    },
    MuiFilledInput: {
        root: {
            backgroundColor: 'rgba(0,0,0,0)'
        }
    }
  },
});

export default themeMaterial(theme);