const user = {
  'user.email': 'Email',
  'user.name': 'Name',
  'user.lastName': 'Last Name',
  'user.password': 'Password',
  'user.repeatPassword': 'Repeat password',
  'user.signIn': 'Sign in',
  'user.email.errorText': 'This email already exists',
  'user.email.helperText': 'Ex: example@shopadvizor.com',
  'user.string.helperText': 'Only characters are valid, max {maxLength}',
  'user.password.helperText': 'At least 8 characters, with at least one digit,on capital letter and one lowecase letter',
  'user.repeatPassword.helperText': 'Should be the same as password',
  'user.acceptConditions.helperText': 'Please, you must accept the conditions',
  'user.activation.success': 'Your account has been activated successfully',
  'user.activation.fail': 'There was a problem with the activation, please, try it later',
  'user.field.mandatory': 'This field is mandatory',
};

export default user;
