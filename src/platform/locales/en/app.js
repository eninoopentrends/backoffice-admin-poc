const app = {
  'app.greeting': 'Hello {name}',
  'app.login': 'Login Page',
  'app.greetingPrivate': 'Private!',
};

export default app;
