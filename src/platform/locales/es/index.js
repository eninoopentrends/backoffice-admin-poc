import app from './app';
import user from './user';
import promotion from './promotion';

export default {
  ...app,
  ...user,
  ...promotion,
};
