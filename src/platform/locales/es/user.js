const user = {
  'user.email': 'Correo electrónico',
  'user.name': 'Nombre',
  'user.lastName': 'Apellidos',
  'user.password': 'Contraseña',
  'user.repeatPassword': 'Confirme contraseña',
  'user.signIn': 'Registrarse',
  'user.email.errorText': 'Este email ya existe',
  'user.email.helperText': 'Ej: example@shopadvizor.com',
  'user.string.helperText': 'Solo se permiten letras, hasta un máximo de {maxLength}',
  'user.password.helperText': 'Al menos 8 carácteres. Debe incluir al menos un número, una letra mayúscula y otra minúscula',
  'user.repeatPassword.helperText': 'Debe coincidir con la contraseña',
  'user.acceptConditions.helperText': 'Por favor, antes de continuar acepta las condiciones',
  'user.activation.success': 'Tu cuenta ha sido activada con éxito',
  'user.activation.fail': 'Lo sentimos, hubo problema en la activación, inténtalo más tarde',
  'user.field.mandatory': 'Este campo es obligatorio',
};

export default user;
