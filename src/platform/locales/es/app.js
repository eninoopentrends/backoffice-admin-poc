const app = {
  'app.greeting': 'Hola {name}',
  'app.login': 'Página Login',
  'app.greetingPrivate': 'Privado!',
};

export default app;
