
export default {
  color: {
    primary: {
      light: '#4d679d',
      main: '#1B3D6E',
      dark: '#001842',
      contrastText: '#FAFAFA',
    },
    secondary: {
      light: '#83E3FF',
      main: '#48B1E9',
      dark: '#0082B7',
      contrastText: '#FAFAFA',
    },
    terciary: {
      light: '#ff759e',
      main: '#e24070',
      dark: '#ab0045',
      contrastText: '#FAFAFA',
    },
    grey: {
      lighter: '#fafafa',
      light: '#f4f4f4',
      medium: '#eeeeee',
      main: '#cccccc',
      dark: '#bcbcbc',
      darker: '#707070',
      black: '#1e2023',
    },
    error: {
      light: '#ff7961',
      main: '#f44336',
      dark: '#ba000d',
      contrastText: '#FAFAFA',
    },
    success: {
      light: '#66d568',
      main: '#2da33a',
      dark: '#007306',
      contrastText: '#FAFAFA',
    },
    warning: {
      light: '#ffd149',
      main: '#ffa000',
      dark: '#c67100',
      contrastText: '#fafafa',
    },
    disabled: {
      color: 'rgba(0, 0, 0, 0.22)',
      backgroundColor: 'rgba(0, 0, 0, 0.025)',
    },
  },
  typography: {
    fontFamily: "'Noto Sans', Arial, 'Helvetica Neue', Helvetica, sans-serif",
    fontSize: 14,
    title: {
      color: '#707070',
      fontFamily: "'Noto Sans', Arial, 'Helvetica Neue', Helvetica, sans-serif",
      lineHeight: '36',
      fontSize: '30',
      fontWeight: '500',
    },
    subheading: {
      color: '#707070',
      fontFamily: "'Noto Sans', Arial, 'Helvetica Neue', Helvetica, sans-serif",
      lineHeight: '24',
      fontSize: '20',
      fontWeight: '500',
    },
    body: {
      color: '#707070',
      fontFamily: "'Noto Sans', Arial, 'Helvetica Neue', Helvetica, sans-serif",
      lineHeight: '1.2',
      fontSize: '14',
      fontWeight: '400',
    },
    h1: {
      fontSize: '30px',
      fontWeight: '500',
    },
    h2: {
      fontSize: '20px',
      fontWeight: '500',
    },
    h3: {
      fontSize: '18px',
      fontWeight: '500',
    },
    h4: {
      fontSize: '16px',
      fontWeight: '500',
    },
    h5: {
      fontSize: '14px',
      fontWeight: '500',
    },
    h6: {
      fontSize: '13px',
      fontWeight: '500',
    },
  },
  background: {
    body: '#f4f4f4',
    pill: '#fafafa',
  },
  button: {
    textTransform: 'none',
    color: '#1B3D6E',
    fontFamily: "'Noto Sans', Arial, 'Helvetica Neue', Helvetica, sans-serif",
    fontSize: '14px',
    fontWeight: '500',
    borderRadius: '4',
    border: '1',
    borderColor: '#1B3D6E66',
    lineHeight: 1.33,
    minWidth: '136px',
  },
  spacing: '1rem',
  shape: {
    borderRadius: 4,
  },
};
